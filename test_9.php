<?php

require_once __DIR__.'/src/Earnberry.php';

Earnberry::createInstance('52711ef64d7642d33e8b4567', '72843ea0e465bb0a175d21a018e84cfaf76b2abe');
//Earnberry::setEncoding('WINDOWS-1251');

Earnberry::getInstance()->setApiUrl('http://earnberry.local/');

try {
    $t = new EarnberryTransactionPurchase();
    //$t->setCurrency('VND');

    //addProduct($sku, $name, $category = '', $brand = '', $variant = '', $position = '')
    $t->setPageview('universal.earnberry.net', '/index.html', '');
    $t->addProduct('12', 'Botinok baba', 'Supercategory', 'Adidas', 'size XXL', 1, 50.25, 2);
    $t->addProduct('13', 'Botinok dead', 'Megacategory', 'Nike', 'size XL', 2, 35.34, 1);
    $t->setAffiliation('Online store');

    //$t->setDryRun(true);

    $t->setTrackingId(56);
    $t->addCustomDimension(1, "Kiev");
    $t->setGrossProfit(50);
    $t->setTotal(25);

    $res = $t->send();

    var_dump($res);
}
catch (EarnberryException $e)
{
    echo $e->getMessage().PHP_EOL;
}