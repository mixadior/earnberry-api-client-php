<?php

require_once __DIR__.'/src/Earnberry.php';

Earnberry::createInstance('52711ef64d7642d33e8b4567', '72843ea0e465bb0a175d21a018e84cfaf76b2abe');
//Earnberry::setEncoding('WINDOWS-1251');

Earnberry::getInstance()->setApiUrl('http://earnberry.local/');

try {
	$t = new EarnberryTransaction();
    //$t->setCurrency('VND');
    //$t->setDryRun(true);
    $t->addProduct('pik-1', 'Aik', 'Tuhlo', 350, 1);
    $t->setTrackingId(93);
    $t->addCustomDimension(1, "Kiev");
    $t->setGrossProfit(50);
    $t->setTotal(25);

    $res = $t->send();
}
catch (EarnberryException $e)
{
    echo $e->getMessage().PHP_EOL;
}